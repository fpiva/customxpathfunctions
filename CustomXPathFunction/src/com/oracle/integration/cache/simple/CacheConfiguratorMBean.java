package com.oracle.integration.cache.simple;

import java.io.IOException;

public interface CacheConfiguratorMBean {

	void flushCache() throws IOException;
	
	
}
