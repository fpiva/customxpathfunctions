package com.oracle.integration.cache.simple;

import com.oracle.integration.cache.Cache;

import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.NodeList;


public class SSimpleCache implements Cache {

	private static SSimpleCache instance = new SSimpleCache();
	
	private Map<String, String> cache = new HashMap<String, String>();
        private Map<String, NodeList> cacheRESB = new HashMap<String, NodeList>();


	private SSimpleCache() {
	}
	
	public static SSimpleCache getInstance() {
		return instance;
	}

	@Override
	public void flushCache() {
		cache.clear();
	}

	@Override
	public String getValue(String key) {
		return cache.get(key);
	}

	@Override
	public void storeValue(String key, String value) {
		cache.put(key, value);
	}
        
        @Override
        public void storeValueRESB(String key, NodeList value) {
          cacheRESB.put(key, value);
        }
    
        @Override
        public NodeList getValueRESB(String key) {
          return cacheRESB.get(key);
        }
}
