package com.oracle.integration.cache.simple;

import com.oracle.integration.cache.SCacheFactory;

import java.io.IOException;


public class CacheConfigurator implements CacheConfiguratorMBean {

	public void flushCache() throws IOException {
		SCacheFactory.getCache().flushCache();
	}

}
