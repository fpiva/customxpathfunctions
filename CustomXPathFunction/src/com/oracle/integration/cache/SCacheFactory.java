package com.oracle.integration.cache;

import com.oracle.integration.cache.simple.SSimpleCache;

public class SCacheFactory {

	public static Cache getCache() {
		return SSimpleCache.getInstance();
	}
}
