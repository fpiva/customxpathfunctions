package com.oracle.integration.cache;

import org.w3c.dom.NodeList;


public interface Cache {


	public String getValue(String key);
        
        public NodeList getValueRESB(String key);
	
	public void storeValue(String key, String value);
        
        public void storeValueRESB(String key, NodeList value);
	
	public void flushCache();
	

}
