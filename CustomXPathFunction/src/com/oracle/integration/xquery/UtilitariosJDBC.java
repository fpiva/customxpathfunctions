package com.oracle.integration.xquery;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class UtilitariosJDBC {


	private static final String DATASOURCE_XA = "jdbc/AuthenticationContext";
	
	
	private static Log logger = LogFactory.getLog(UtilitariosJDBC.class);
	
	private static Connection getXAConnection() throws SQLException {
		return System.getProperty("test.db") != null ? getTestConnection() : getConnectionFromDataSource(DATASOURCE_XA);	
	}

//	private static Connection getNonXAConnection() throws SQLException {
//		return System.getProperty("test.db") != null ? getTestConnection() : getConnectionFromDataSource(HALIX_DATASOURCE_NON_XA);
//	}

	private static Connection getTestConnection() throws SQLException {
		Connection conn = null;
		try {
			Class.forName("oracle.jdbc.OracleDriver");
			//
			conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE","DTVLA","DTVLA");
		} catch (ClassNotFoundException e) {
			logger.error("Error instanciando el driver",e);
			throw new SQLException(e.getMessage());
		}
		return conn;
	}

	private static Connection getConnectionFromDataSource(String dsName) throws SQLException {
		Context ctx = null;
		Connection conn = null;
		try {
			conn = null;
			ctx = new InitialContext();
			javax.sql.DataSource ds = (javax.sql.DataSource) ctx
					.lookup(dsName);
			conn = ds.getConnection();
		} catch (NamingException e) {
			logger.error("Error obteniendo el contexto",e);
			throw new SQLException(e.getMessage());
		}
		return conn;
	}

	
			
	public static void closeAll(Connection con, Statement stm, ResultSet rs) {
		if (stm != null) {
			try {
				stm.close();
			} catch (SQLException e) {
				logger.warn("Error cerrando Statement", e);
			}
		}
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				logger.warn("Error cerrando ResultSet", e);
			}
		}
		if (con != null) {
			try {
				con.close();
			} catch (SQLException e) {
				logger.warn("Error cerrando conexion", e);
			}
		}
	}

	public static Connection getConnection() throws SQLException {
		return getXAConnection();
	}

}