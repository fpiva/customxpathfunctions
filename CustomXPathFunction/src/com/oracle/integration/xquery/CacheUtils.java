package com.oracle.integration.xquery;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.naming.InitialContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.oracle.integration.cache.Cache;
import com.oracle.integration.cache.SCacheFactory;
import com.oracle.integration.cache.simple.CacheConfigurator;
import com.oracle.integration.cache.simple.CacheConfiguratorMBean;


public class CacheUtils {


	private static Log logger = LogFactory.getLog(CacheUtils.class);

	static {
		testConnection();
		registerConfigurationMBean();
		getDataValues();
	}


	private static void testConnection() {
		Connection connection = null;
		PreparedStatement stm = null;
		ResultSet rs = null;
		try {
			connection = UtilitariosJDBC.getConnection();
			stm = connection.prepareStatement("SELECT 1 FROM DUAL");
		
			rs = stm.executeQuery();


		} catch (SQLException e) {
			logger.error(e);
			e.printStackTrace();
		} finally {
			UtilitariosJDBC.closeAll(connection, stm, rs);
		}
	}
	

	private static void registerConfigurationMBean() {
        try {
			InitialContext initialContext = new InitialContext();
			MBeanServer mbs = MBeanServer.class.cast(initialContext.lookup("java:comp/jmx/runtime"));
			CacheConfiguratorMBean config = new CacheConfigurator();
			ObjectName oname = new ObjectName("com.oracle.integration.cache:type=CacheConfiguratorMBean,name=CacheConfig");
			mbs.registerMBean(config, oname);
			logger.info("========== CacheConfiguratorMBean registered!");
        
        } catch (Exception e) {
        	logger.error("========== Error registering CacheConfiguratorMBean", e);
        	e.printStackTrace();
        }
        
	}

	public static void getDataValues() {
		Cache cache = SCacheFactory.getCache();
		

		Connection connection = null;
		PreparedStatement stm = null;
		ResultSet rs = null;
		String pais = null;
		String instancia = null;
		String servicio = null;
		String operacion = null;
		String ttl = null;
		try {
			String sql ="SELECT COUNTRY,INSTANCE,SERVICE,OPERATION,TTL " +
						"FROM CACHE_CONFIGURATION";

			
			
			connection = UtilitariosJDBC.getConnection();
			stm = connection.prepareStatement(sql);
			rs = stm.executeQuery();
			while( rs.next()) {
				pais = rs.getString("COUNTRY");
				instancia = rs.getString("INSTANCE");
				servicio = rs.getString("SERVICE");
				operacion = rs.getString("OPERATION");
				ttl = rs.getString("TTL");
				cache.storeValue(pais+"-"+instancia+"-"+servicio+"-"+operacion, ttl);
				logger.info("Se han agregado al cache los valores Servicio-Operación: "+pais+"-"+instancia+"-"+servicio+"-"+operacion+" TTL:" + ttl);
			}

		} catch (SQLException e) {
        	logger.error("========== Error cargando datos en la tabla CACHE_CONFIGURATOR", e);
			e.printStackTrace();
		} finally {
			UtilitariosJDBC.closeAll(connection, stm, rs);
		}
		
		
	}
	public static String getOperationTTL(String servicio, String operacion) {
		Cache cache = SCacheFactory.getCache();
		
		String pais = "AR";
		String instancia ="DEFAULT";
		String key = pais + "-" + instancia + "-" + servicio + "-" + operacion;
		String resultadoCache = (String) cache.getValue(key);
		
		if (resultadoCache == null) {
			resultadoCache = "-1";
		}
//			long resultado = Long.getLong(resultadoCache).longValue();
		return resultadoCache;
	}
	
	public static String getOperationTTLV2(String pais, String instancia, String servicio, String operacion) {
		Cache cache = SCacheFactory.getCache();
		
		String key = pais + "-" + instancia + "-" + servicio + "-" + operacion;
		String resultadoCache = (String) cache.getValue(key);
		
		if (resultadoCache == null) {
			resultadoCache = "-1";
		}
//			long resultado = Long.getLong(resultadoCache).longValue();
		return resultadoCache;
	}

	public static long getPayloadHashS(String payload) {
		
		long hash = payload.hashCode();
		//logger.error("payload:" + payload);
		return hash;
	}
	
}
