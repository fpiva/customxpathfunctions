package oracle.fabric.common.xml.xpath;

import com.oracle.integration.cache.Cache;
import com.oracle.integration.cache.SCacheFactory;
import com.oracle.integration.cache.simple.CacheConfigurator;
import com.oracle.integration.cache.simple.CacheConfiguratorMBean;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import javax.management.MBeanServer;
import javax.management.ObjectName;

import javax.naming.InitialContext;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

public class DomainValueMapper {

	private static Log logger = LogFactory.getLog(DomainValueMapper.class);
	private static final String nullValue = "NULL_VALUE";
	

	private static String getValueXpath(String valueXpath){
		if (valueXpath==null || valueXpath.length()==0 || valueXpath.trim().length()==0)		
			return nullValue;
		else
			return valueXpath;	
	}
	
	static {
		testConnection();
		registerConfigurationMBean();
	}

	private static void testConnection() {
		Connection connection = null;
		PreparedStatement stm = null;
		ResultSet rs = null;
		try {
			connection = UtilitariosJDBC.getConnection();
			stm = connection.prepareStatement("SELECT 1 FROM DUAL");

			rs = stm.executeQuery();

		} catch (SQLException e) {
			logger.error(e);
			e.printStackTrace();
		} finally {
			UtilitariosJDBC.closeAll(connection, stm, rs);
		}
	}

	private static void registerConfigurationMBean() {
		try {
			InitialContext initialContext = new InitialContext();
			MBeanServer mbs = MBeanServer.class.cast(initialContext
					.lookup("java:comp/env/jmx/runtime"));
			CacheConfiguratorMBean config = new CacheConfigurator();
			ObjectName oname = new ObjectName(
					"com.oracle.integration.cache:type=CacheConfiguratorMBean,name=CacheConfig");
			mbs.registerMBean(config, oname);
			logger.info("========== CacheConfiguratorMBean registered!");

		} catch (Exception e) {
			logger.error("========== Error registering CacheConfiguratorMBean",
					e);
			e.printStackTrace();
		}

	}

	public static String flushCache(String nombreCache) {
		nombreCache = "OK";
		Cache cache = SCacheFactory.getCache();
		cache.flushCache();
		return "OK";
	}

	public static String lookUpCanonicalCode(String entidadCanonica,
			String sistema, String pais, String valorSistema) {
		Cache cache = SCacheFactory.getCache();
		String key = "DVM_" + entidadCanonica + "_" + sistema + "_" + pais
				+ "_" + valorSistema;
		String resultado = (String) cache.getValue(key);

		if (resultado == null) {
			resultado = lookUpCanonicalCodeFromDB(entidadCanonica, sistema,
					pais, valorSistema);

			if (resultado != null) {
				cache.storeValue(key, resultado);
				logger.info("Se ha agregado al cache el parámetro de la propiedad ("
						+ key + "): " + resultado);
			} else {
				throw new NoSuchElementException(key);
			}
		}

		return resultado;
	}

	public static String lookUpCanonicalCode_MRIBS(String entidadCanonica,
			String sistema, String pais, String valorSistema) {
		Cache cache = SCacheFactory.getCache();
		String key = "DVM_" + entidadCanonica + "_" + sistema + "_" + pais
				+ "_" + getValueXpath(valorSistema);
		String resultado = (String) cache.getValue(key);

		if (resultado == null) {
			resultado = MRIBS_lookUpCanonicalCodeFromDB(entidadCanonica,
					sistema, pais, valorSistema);

			if (resultado != null) {
				cache.storeValue(key, resultado);
				logger.info("Se ha agregado al cache el parámetro de la propiedad ("
						+ key + "): " + resultado);
			} else {
				throw new NoSuchElementException(key);
			}
		}

		return resultado;
	}

	public static String lookUpSystemCode_MRIBS(String entidadCanonica,
			String sistema, String pais, String valorCanonico) {
		Cache cache = SCacheFactory.getCache();
		
		String key = "DVM_" + entidadCanonica + "_" + sistema + "_" + pais
				+ "_" + getValueXpath(valorCanonico)
				;
		String resultado = (String) cache.getValue(key);

		if (resultado == null) {
			resultado = MRIBS_lookUpSystemCodeFromDB(entidadCanonica, sistema,
					pais, valorCanonico);

			
			if (resultado != null) {
				cache.storeValue(key, resultado);
				logger.info("Se ha agregado al cache el parámetro de la propiedad ("
						+ key + "): " + resultado);
			} else {
				throw new NoSuchElementException(key);
			}

		}

		return resultado;
	}

	private static String MRIBS_lookUpCanonicalCodeFromDB(
			String entidadCanonica, String sistema, String pais, String valor) {

		Connection connection = null;
		PreparedStatement stm = null;
		ResultSet rs = null;
		String resultado = null;

		try {

			String sql = "SELECT "
					+ "VALOR_CANONICO "	+ "FROM " + "DVM_SISTEMA_MRIBS SYS, DVM_VALOR_SISTEMA_MRIBS VS, DVM_VALOR_CANONICO_MRIBS VC, DVM_ENTIDAD_CANONICA_MRIBS ENT "
					+ "WHERE " + "ENT.NOMBRE = '" + entidadCanonica + "' "
					+ "AND " + "ENT.ID = VC.ID_ENTIDAD_CANONICA " + "AND "
					+ "SYS.NOMBRE = '" + sistema + "' " + "AND "
					+ "SYS.PAIS = '" + pais + "' " + "AND "
					+ "SYS.ID = ID_SISTEMA " + "AND " + "VALOR_SISTEMA = '"
					+ valor + "' " + "AND " + "VC.ID = VS.ID_VALOR_CANONICO";

			connection = UtilitariosJDBC.getConnection();
			stm = connection.prepareStatement(sql);
			rs = stm.executeQuery();
			if (rs.next()) {
				if (rs.getString("VALOR_CANONICO") == null)
					resultado = "";
				else	
					resultado = rs.getString("VALOR_CANONICO");
			}

		} catch (SQLException e) {
			logger.error(e);
			e.printStackTrace();
		} finally {
			UtilitariosJDBC.closeAll(connection, stm, rs);
		}
		return resultado;
	}

	private static String MRIBS_lookUpSystemCodeFromDB(String entidadCanonica,
			String sistema, String pais, String valor) {

		Connection connection = null;
		PreparedStatement stm = null;
		ResultSet rs = null;
		String resultado = null;
		String sql = null;
		
		try {

			sql = "SELECT "
					+ "VALOR_SISTEMA "
					+ "FROM "
					+ "DVM_SISTEMA_MRIBS SYS, DVM_VALOR_SISTEMA_MRIBS VS, DVM_VALOR_CANONICO_MRIBS VC, DVM_ENTIDAD_CANONICA_MRIBS ENT "
					+ "WHERE " + "ENT.NOMBRE = '" + entidadCanonica + "' "
					+ "AND " + "ENT.ID = VC.ID_ENTIDAD_CANONICA " + "AND "
					+ "SYS.NOMBRE = '" + sistema + "' " + "AND "
					+ "SYS.PAIS = '" + pais + "' " + "AND "
					+ "SYS.ID = ID_SISTEMA " + "AND " + "nvl(VALOR_CANONICO,'" + nullValue + "') = '"
					+ getValueXpath(valor) + "' " + "AND " + "VC.ID = VS.ID_VALOR_CANONICO";
			
			
			connection = UtilitariosJDBC.getConnection();
			stm = connection.prepareStatement(sql);
			rs = stm.executeQuery();
			if (rs.next()) {
				resultado = rs.getString("VALOR_SISTEMA");
			}

		} catch (SQLException e) {
			logger.error(e);
			e.printStackTrace();
		} finally {
			UtilitariosJDBC.closeAll(connection, stm, rs);
		}
		return resultado;
		
	}

	public static String lookUpSystemCode(String entidadCanonica,
			String sistema, String pais, String valorCanonico) {
		Cache cache = SCacheFactory.getCache();
		String key = "DVM_" + entidadCanonica + "_" + sistema + "_" + pais
				+ "_" + valorCanonico;
		String resultado = (String) cache.getValue(key);

		if (resultado == null) {
			resultado = lookUpSystemCodeFromDB(entidadCanonica, sistema, pais,
					valorCanonico);

			if (resultado != null) {
				cache.storeValue(key, resultado);
				logger.info("Se ha agregado al cache el parámetro de la propiedad ("
						+ key + "): " + resultado);
			} else {
				throw new NoSuchElementException(key);
			}

		}

		return resultado;
	}

	private static String lookUpCanonicalCodeFromDB(String entidadCanonica,
			String sistema, String pais, String valor) {

		Connection connection = null;
		PreparedStatement stm = null;
		ResultSet rs = null;
		String resultado = null;

		try {

			String sql = "SELECT "
					+ "VALOR_CANONICO "
					+ "FROM "
					+ "DVM_SISTEMA SYS, DVM_VALOR_SISTEMA VS, DVM_VALOR_CANONICO VC, DVM_ENTIDAD_CANONICA ENT "
					+ "WHERE " + "ENT.NOMBRE = '" + entidadCanonica + "' "
					+ "AND " + "ENT.ID = VC.ID_ENTIDAD_CANONICA " + "AND "
					+ "SYS.NOMBRE = '" + sistema + "' " + "AND "
					+ "SYS.PAIS = '" + pais + "' " + "AND "
					+ "SYS.ID = ID_SISTEMA " + "AND " + "VALOR_SISTEMA = '"
					+ valor + "' " + "AND " + "VC.ID = VS.ID_VALOR_CANONICO";

			connection = UtilitariosJDBC.getConnection();
			stm = connection.prepareStatement(sql);
			rs = stm.executeQuery();
			if (rs.next()) {
				resultado = rs.getString("VALOR_CANONICO");
			}

		} catch (SQLException e) {
			logger.error(e);
			e.printStackTrace();
		} finally {
			UtilitariosJDBC.closeAll(connection, stm, rs);
		}
		return resultado;
	}

	private static String lookUpSystemCodeFromDB(String entidadCanonica,
			String sistema, String pais, String valor) {

		Connection connection = null;
		PreparedStatement stm = null;
		ResultSet rs = null;
		String resultado = null;

		try {

			String sql = "SELECT "
					+ "VALOR_SISTEMA "
					+ "FROM "
					+ "DVM_SISTEMA SYS, DVM_VALOR_SISTEMA VS, DVM_VALOR_CANONICO VC, DVM_ENTIDAD_CANONICA ENT "
					+ "WHERE " + "ENT.NOMBRE = '" + entidadCanonica + "' "
					+ "AND " + "ENT.ID = VC.ID_ENTIDAD_CANONICA " + "AND "
					+ "SYS.NOMBRE = '" + sistema + "' " + "AND "
					+ "SYS.PAIS = '" + pais + "' " + "AND "
					+ "SYS.ID = ID_SISTEMA " + "AND " + "VALOR_CANONICO = '"
					+ valor + "' " + "AND " + "VC.ID = VS.ID_VALOR_CANONICO";

			connection = UtilitariosJDBC.getConnection();
			stm = connection.prepareStatement(sql);
			rs = stm.executeQuery();
			if (rs.next()) {
				resultado = rs.getString("VALOR_SISTEMA");
			}

		} catch (SQLException e) {
			logger.error(e);
			e.printStackTrace();
		} finally {
			UtilitariosJDBC.closeAll(connection, stm, rs);
		}
		return resultado;
	}
	
  public static Object lookUpHomologationByCanonicalCode(String sourceID, String canonicalCode) {

	    Cache cache = SCacheFactory.getCache();
	    String key = "DVMRESB_" + sourceID + "_" + canonicalCode;
	    NodeList resultado = cache.getValueRESB(key);

	    if (resultado == null) {
	        HomologationData homologationData = null;
	        homologationData = lookUpHomologationByCanonicalCodeFromDB(sourceID, canonicalCode);
	        
	        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
	        DocumentBuilder documentBuilder = null;
	        try {
	            documentBuilder = documentBuilderFactory.newDocumentBuilder();
	        } catch (ParserConfigurationException e) {
	            logger.error(e);
	            e.printStackTrace();
	        }
	        DOMImplementation domImplementation = documentBuilder.getDOMImplementation();
	        //Create root GetHomologationByCanonicalCodeResult
	        Document document = domImplementation.createDocument("http://directvla.com/schema/businessdomain/Utility/v1-0","GetHomologationByCanonicalCodeResult", null);
	        
	        //Get NodeList
	        resultado = document.getElementsByTagName("GetHomologationByCanonicalCodeResult");
	        
	        if (homologationData != null) {
	          Element elemHomologation = document.getDocumentElement();
	          buildHomologationBodyXML(document, elemHomologation, homologationData);
	          //Get NodeList
	          resultado = document.getElementsByTagName("GetHomologationByCanonicalCodeResult");
	          //Save in Cache
	          cache.storeValueRESB(key, resultado);
	          logger.info("Se ha agregado al cache el parámetro de la propiedad (" + key + "): " + resultado);
	        }
	    }

	    return resultado;
	  }
	  
	  private static HomologationData lookUpHomologationByCanonicalCodeFromDB(String sourceID, String canonicalCode) {

	      Connection connection = null;
	      PreparedStatement stm = null;
	      ResultSet rs = null;
	      HomologationData homologationData = null;

	      try {

	          String sql = "SELECT COUNTRYID, CANONICALCODE, TARGETSYSTEMCODE, HOMOLOGATEDCONCEPT, HOMOLOGATEDCODE, CATEGORYID " +
	              "FROM DTVLA.TBL_HOMOLOGATIONDATA WHERE COUNTRYID = upper('" +
	              sourceID + "') AND CANONICALCODE = '"+ canonicalCode + "'";
	        
	          connection = UtilitariosJDBC.getConnection();
	          stm = connection.prepareStatement(sql);
	          rs = stm.executeQuery();
	        
	          while (rs.next()) {
	            homologationData = new HomologationData();
	            homologationData.setCountryID(rs.getString("COUNTRYID"));
	            homologationData.setCanonicalCode(rs.getString("CANONICALCODE"));
	            homologationData.setTargetSystemCode(rs.getString("TARGETSYSTEMCODE"));
	            homologationData.setHomologatedConcept(rs.getString("HOMOLOGATEDCONCEPT"));
	            homologationData.setHomologatedCode(rs.getString("HOMOLOGATEDCODE"));
	            homologationData.setCategoryID(rs.getInt("CATEGORYID"));
	          }

	      } catch (SQLException e) {
	          logger.error(e);
	          e.printStackTrace();
	      } finally {
	          UtilitariosJDBC.closeAll(connection, stm, rs);
	      }
	      return homologationData;
	  }
	  
	  public static Object lookUpHomologationCollectionByCanonicalCategoryCode(String sourceID, String canonicalCode) {
	    
	    Cache cache = SCacheFactory.getCache();
	    String key = "DVMRESB_COLLECTION" + sourceID + "_" + canonicalCode;
	    NodeList resultado = cache.getValueRESB(key);

	    if (resultado == null) {
	        List<HomologationData> homologationDataList = null;
	        homologationDataList = lookUpHomologationCollectionByCanonicalCategoryCodeFromDB(sourceID, canonicalCode);
	        
	        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
	        DocumentBuilder documentBuilder = null;
	        try {
	            documentBuilder = documentBuilderFactory.newDocumentBuilder();
	        } catch (ParserConfigurationException e) {
	            logger.error(e);
	            e.printStackTrace();
	        }
	        DOMImplementation domImplementation = documentBuilder.getDOMImplementation();
	        //Create root GetHomologationCollectionByCanonicalCategoryCodeResult
	        Document document = domImplementation.createDocument("http://directvla.com/schema/businessdomain/Utility/v1-0","GetHomologationCollectionByCanonicalCategoryCodeResult", null);
	        
	        Element elemHomologations = document.createElementNS("http://directvla.com/schema/businessdomain/Utility/v1-0","utility-v1-0:Homologations");
	        document.getDocumentElement().appendChild(elemHomologations);
	        
	        //get NodeList
	        resultado = document.getElementsByTagName("GetHomologationCollectionByCanonicalCategoryCodeResult");
	      
	        if (homologationDataList.size() > 0) {
	          
	          Iterator<HomologationData> it = homologationDataList.iterator();
	          while (it.hasNext()) {
	            HomologationData homologationData = it.next();
	            //Element Homologation
	            Element elemHomologation = document.createElementNS("http://directvla.com/schema/util/enterprisehomologationdatatypes/v1-0","enterpriseHomologation-v1-0:Homologation");
	            document.getDocumentElement().appendChild(elemHomologation);
	            buildHomologationBodyXML(document, elemHomologation, homologationData);  
	            //Homologation in Homologations
	            elemHomologations.appendChild(elemHomologation);
	          }
	          //get NodeList
	          resultado = document.getElementsByTagName("GetHomologationCollectionByCanonicalCategoryCodeResult");
	          //Save in Cache
	          cache.storeValueRESB(key, resultado);
	          logger.info("Se ha agregado al cache el parámetro de la propiedad (" + key + "): " + resultado);
	        }
	    }
	    
	    return resultado;
	  }
	  
	  private static List<HomologationData> lookUpHomologationCollectionByCanonicalCategoryCodeFromDB(String sourceID, String canonicalCode) {

	      Connection connection = null;
	      PreparedStatement stm = null;
	      ResultSet rs = null;
	      List<HomologationData> homologationDataList = new ArrayList<HomologationData>();

	      try {

	          String sql = "SELECT t0.COUNTRYID,t0.CANONICALCODE,t0.TARGETSYSTEMCODE,t0.HOMOLOGATEDCONCEPT,t0.HOMOLOGATEDCODE,t0.CATEGORYID " +
	              "FROM TBL_HOMOLOGATIONDATA t0, TBL_HOMOLOGATIONCATEGORIES t1 WHERE (((t0.COUNTRYID = upper('" +
	              sourceID + "')) ) AND (t1.CANONICALCATEGORYCODE = '"+ canonicalCode   + "')  AND (t1.CATEGORYID = t0.CATEGORYID))";
	        
	          connection = UtilitariosJDBC.getConnection();
	          stm = connection.prepareStatement(sql);
	          rs = stm.executeQuery();
	        
	          while (rs.next()) {
	            HomologationData homologationData = new HomologationData();
	            homologationData.setCountryID(rs.getString("COUNTRYID"));
	            homologationData.setCanonicalCode(rs.getString("CANONICALCODE"));
	            homologationData.setTargetSystemCode(rs.getString("TARGETSYSTEMCODE"));
	            homologationData.setHomologatedConcept(rs.getString("HOMOLOGATEDCONCEPT"));
	            homologationData.setHomologatedCode(rs.getString("HOMOLOGATEDCODE"));  
	            homologationData.setCategoryID(rs.getInt("CATEGORYID"));
	            
	            homologationDataList.add(homologationData);
	          }

	      } catch (SQLException e) {
	          logger.error(e);
	          e.printStackTrace();
	      } finally {
	          UtilitariosJDBC.closeAll(connection, stm, rs);
	      }
	      return homologationDataList;
	  }
	  
	  private static void buildHomologationBodyXML(Document document, Element element, HomologationData homologationData) {
	    
	    //Element countryID
	    Element elemCountryID = document.createElementNS("http://directvla.com/schema/util/enterprisehomologationdatatypes/v1-0","enterpriseHomologation-v1-0:countryID");
	    Text textCountryID = document.createTextNode(homologationData.getCountryID());
	    elemCountryID.appendChild(textCountryID);
	    element.appendChild(elemCountryID);
	    //Element canonicalCode
	    Element elemCanonicalCode = document.createElementNS("http://directvla.com/schema/util/enterprisehomologationdatatypes/v1-0","enterpriseHomologation-v1-0:canonicalCode");
	    Text textCanonicalCode = document.createTextNode(homologationData.getCanonicalCode());
	    elemCanonicalCode.appendChild(textCanonicalCode);
	    element.appendChild(elemCanonicalCode);
	    //Element homologatedConcept
	    Element elemHomologatedConcept = document.createElementNS("http://directvla.com/schema/util/enterprisehomologationdatatypes/v1-0","enterpriseHomologation-v1-0:homologatedConcept");
	    Text textHomologatedConcept = document.createTextNode(homologationData.getHomologatedConcept());
	    elemHomologatedConcept.appendChild(textHomologatedConcept);
	    element.appendChild(elemHomologatedConcept);
	    //Element homologatedCode
	    Element elemHomologatedCode = document.createElementNS("http://directvla.com/schema/util/enterprisehomologationdatatypes/v1-0","enterpriseHomologation-v1-0:homologatedCode");
	    Text textHomologatedCode = document.createTextNode(homologationData.getHomologatedCode());
	    elemHomologatedCode.appendChild(textHomologatedCode);
	    element.appendChild(elemHomologatedCode);
	    //Element targetSystemCode
	    Element elemTargetSystemCode = document.createElementNS("http://directvla.com/schema/util/enterprisehomologationdatatypes/v1-0","enterpriseHomologation-v1-0:targetSystemCode");
	    Text textTargetSystemCode = document.createTextNode(homologationData.getTargetSystemCode());
	    elemTargetSystemCode.appendChild(textTargetSystemCode);
	    element.appendChild(elemTargetSystemCode);
	    //Element categoryID
	    Element elemCategoryID = document.createElementNS("http://directvla.com/schema/util/enterprisehomologationdatatypes/v1-0","enterpriseHomologation-v1-0:categoryID");
	    Text textCategoryID = document.createTextNode(String.valueOf(homologationData.getCategoryID()));
	    elemCategoryID.appendChild(textCategoryID);
	    element.appendChild(elemCategoryID);

	  }
		  
}
