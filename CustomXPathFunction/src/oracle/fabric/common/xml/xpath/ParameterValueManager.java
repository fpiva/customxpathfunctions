package oracle.fabric.common.xml.xpath;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.oracle.integration.cache.Cache;
import com.oracle.integration.cache.SCacheFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class ParameterValueManager {
    private static Log logger = LogFactory.getLog(ParameterValueManager.class);
    
    public static String getParameterValueFromDB(String category, String parameterName, String defaultValue) {
                    Connection connection = null;
                    PreparedStatement stm = null;
                    ResultSet rs = null;
                    String resultado = null;
                    try {
                            connection = UtilitariosJDBC.getConnection();
                            String sql = "SELECT " +
                                                            "P.VALOR " +
                                                     "FROM " +
                                                            "PM_PARAMETROS P, PM_CATEGORIAS G " +
                                                      "WHERE " +
                                                            "P.NOMBRE_PARAMETRO = ? " +
                                                      "AND " +
                                                            "G.NOMBRE = ? " +
                                                      "AND G.ID = P.CATEGORIA_ID";
                            
                            stm = connection.prepareStatement(sql);
                            stm.setString(1, parameterName);
                            stm.setString(2, category);
                            
                            rs = stm.executeQuery();
                            while (rs.next()) {
                                    resultado = rs.getString("VALOR");
                            }
                            
                            if(resultado == null){
                                    resultado = defaultValue;
                                    logger.warn("Se ha utilizado el parámetro Default de la propiedad (" + parameterName + "): " + defaultValue);
                            }
                            
                    } catch (SQLException e) {
                            logger.error(e);
                            e.printStackTrace();
                    } finally {
                            UtilitariosJDBC.closeAll(connection, stm, rs);
                    }
                    return resultado;
            }

            public static String getParameterValue(String categoria, String nombreParametro, String valorDefault) {
            
                    Cache cache = SCacheFactory.getCache();
                    
                    String key = "PM_" + categoria + "_" + nombreParametro;
                    String resultado = (String) cache.getValue(key);                
                    
                    if (resultado == null) {
                            resultado = getParameterValueFromDB(categoria, nombreParametro, valorDefault);
                            
                            if (resultado != null) {
                                    cache.storeValue(key, resultado);
                                    logger.info("Se ha agregado al cache el parámetro de la propiedad (" +key + "): " + resultado);
                            } else {
                                    resultado = valorDefault;
                            }
                    }

                    return resultado;
            }
            
}
