package oracle.fabric.common.xml.xpath;

import java.util.UUID;

public class GUIDGenerator {
    static String generateGUID() {
        return UUID.randomUUID().toString();
    }
}
