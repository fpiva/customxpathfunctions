package oracle.fabric.common.xml.xpath;

import com.oracle.integration.cache.Cache;
import com.oracle.integration.cache.SCacheFactory;
import com.oracle.integration.cache.simple.CacheConfigurator;
import com.oracle.integration.cache.simple.CacheConfiguratorMBean;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLDataException;
import java.sql.SQLException;

import java.sql.Timestamp;

import java.util.Date;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Map;
import java.util.NoSuchElementException;

import javax.management.MBeanServer;
import javax.management.ObjectName;

import javax.naming.InitialContext;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.management.RuntimeErrorException;

import javax.naming.SizeLimitExceededException;


public class CrossReferences {
    private static Log logger = LogFactory.getLog(CrossReferences.class);
    private static final String XREF_DS = "jdbc/xref";

    static {
        //testConnection();
      //  registerConfigurationMBean();
    }


    private static void testConnection() {
        Connection connection = null;
        PreparedStatement stm = null;
        ResultSet rs = null;
        try {
            connection = UtilitariosJDBC.getConnection();
            stm = connection.prepareStatement("SELECT 1 FROM DUAL");

            rs = stm.executeQuery();


        } catch (SQLException e) {
            logger.error(e);
            e.printStackTrace();
        } finally {
            UtilitariosJDBC.closeAll(connection, stm, rs);
        }
    }


    private static void registerConfigurationMBean() {
        try {
            InitialContext initialContext = new InitialContext();
            MBeanServer mbs =
                MBeanServer.class.cast(initialContext.lookup("java:comp/env/jmx/runtime"));
            CacheConfiguratorMBean config = new CacheConfigurator();
            ObjectName oname =
                new ObjectName("com.oracle.integration.cache:type=CacheConfiguratorMBean,name=CacheConfig");
            mbs.registerMBean(config, oname);
            logger.info("========== CacheConfiguratorMBean registered!");

        } catch (Exception e) {
            logger.error("========== Error registering CacheConfiguratorMBean",
                         e);
            e.printStackTrace();
        }

    }

    public static String flushCache(String nombreCache) {
        nombreCache = "OK";
        Cache cache = SCacheFactory.getCache();
        cache.flushCache();
        return "OK";
    }

    public static String lookupDefaultXRef(String xrefLocation,
                                           String referenceColumnName,
                                           String referenceValue,
                                           String columnName,
                                           boolean needException) throws Exception {

        //Cache cache = SCacheFactory.getCache();
        //String nombreTabla[] = tabla.split("/");
        //String nombreTablaDB = "XREF_"+nombreTabla[nombreTabla.length-1].substring(0, nombreTabla[nombreTabla.length-1].length()-5).toUpperCase() ;
        String nombreTablaDB = getTableName(xrefLocation);
        String key =
            "XREF_" + referenceColumnName + "_" + nombreTablaDB + "_" +
            columnName + "_" + referenceValue;

        String resultado = null;
        if (resultado == null) {
            try {
                resultado =
                        lookupDefaultXRefFromDB(nombreTablaDB, referenceColumnName,
                                                referenceValue, columnName);

                if (resultado != null) {
                    //  cache.storeValue(key, resultado);
                    logger.info("Se ha obtenido al cache el parámetro de la propiedad (" +
                                key + "): " + resultado);
                } else {
                    if (needException)
                        throw new NoSuchElementException(key);
                }
            } catch (Exception e) {
                if (needException)
                    throw e;
            }
        }

        return resultado;
    }


    public static String lookupXRef(String xrefLocation,
                                    String referenceColumnName,
                                    String referenceValue, String columnName,
                                    boolean needException) throws Exception {

        String nombreTablaDB = getTableName(xrefLocation);
        String key =
            "XREF_" + referenceColumnName + "_" + nombreTablaDB + "_" +
            columnName + "_" + referenceValue;

        
        String resultado = null;
       
            try {
                resultado =
                        lookupXrefFromDB(nombreTablaDB, referenceColumnName,
                                         referenceValue, columnName);

                if (resultado != null) {
                    //  cache.storeValue(key, resultado);
                    logger.info("Se ha obtenido al cache el parámetro de la propiedad (" +
                                key + "): " + resultado);
                } else {
                    if (needException)
                        throw new NoSuchElementException(key);
                }
            } catch (Exception e) {
                if (needException)
                    throw e;
            }
       

        return resultado;
    }


    public static String populateXRefRow(String xrefLocation,
                                          String referenceColumnName,
                                          String referenceValue,
                                          String columnName, String value,
                                          String mode) throws SQLException {

        String tabla = getTableName(xrefLocation);
        String sql = null;
        String rownumber = GUIDGenerator.generateGUID();
       
        if (mode.equals("ADD")) {
            sql= insertXRefRow(tabla, referenceColumnName, referenceValue, columnName, value, null, mode);

        } else if (mode.equals("LINK")) {
            sql = updateXRefRow( tabla,  referenceColumnName, referenceValue,  columnName, value,  rownumber);
        } else if (mode.equals("UPDATE")) {
            sql = updateXRefRow( tabla,  referenceColumnName, referenceValue,  columnName, value,  rownumber);
        } else {
            throw new InputMismatchException();
        }

        
        return sql;
    }


    private static String lookupXrefFromDB(String tabla, String sistemaOrigen,
                                           String valor, String sistemaDestino,
                                           Connection connection) throws SizeLimitExceededException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        String resultado = null;
        try {

            String sql =
                "SELECT " + sistemaDestino + " FROM " + tabla + " WHERE " +
                sistemaOrigen + " = '" + valor + "'";
            System.out.println(sql);
            stm = connection.prepareStatement(sql);
            rs = stm.executeQuery();
            while (rs.next()) {
                if (rs.getRow() > 1) {
                    throw new SizeLimitExceededException("Multiple values found for: " +
                                                         tabla + "_" +
                                                         sistemaOrigen + "_" +
                                                         valor);
                }
                resultado = rs.getString(sistemaDestino);
            }
          
        } catch (SQLException e) {
            logger.error(e);

        } finally {
            UtilitariosJDBC.closeStatement(stm);
            UtilitariosJDBC.closeRs(rs);
        }
        return resultado;
    }


    private static String lookupDefaultXRefFromDB(String tabla,
                                                  String sistemaOrigen,
                                                  String valor,
                                                  String sistemaDestino) throws SQLException {
        Connection connection = null;
        String resultado = null;
        try {
            connection = getConnection();
            try {
                resultado =
                        lookupXrefFromDB(tabla, sistemaOrigen, valor, sistemaDestino,
                                         connection);
            } catch (SizeLimitExceededException e) {
                logger.debug(e.getMessage());
                resultado =
                        lookupAuxXrefFromDB(tabla, sistemaOrigen, valor, sistemaDestino,
                                            connection);
            }
        } catch (SQLException e) {
            logger.error(e);

            throw e;
        } finally {
            UtilitariosJDBC.closeConnection(connection);
        }
        return resultado;
    }


  private static String lookupAuxXrefFromDB(String tabla,
                                              String sistemaOrigen,
                                              String valor,
                                              String sistemaDestino,
                                              Connection connection) throws SQLException {
        PreparedStatement stm = null;
        ResultSet rs = null;
        String resultado = null;
        try {
            String sql =
                "SELECT " + sistemaDestino + " FROM " + tabla + "_AUX WHERE " +
                sistemaOrigen + " = '" + valor + "'";
            stm = connection.prepareStatement(sql);
            rs = stm.executeQuery();
            if (rs.next()) {
                resultado = rs.getString(sistemaDestino);
            }
            return resultado;
        } finally {
            UtilitariosJDBC.closeStatement(stm);
            UtilitariosJDBC.closeRs(rs);
        }
    } 




    private static String getTableName(String xref) {
        Pattern exp = Pattern.compile("([^/]+)\\.xref");
        Matcher matcher = exp.matcher(xref);
        matcher.find();
        return "XREF_" + matcher.group(1).toUpperCase();
    }

    private static String lookupXrefFromDB(String nombreTablaDB,
                                           String referenceColumnName,
                                           String referenceValue,
                                           String columnName) throws SQLException, SizeLimitExceededException {
        Connection connection = getConnection();
        try {


            return lookupXrefFromDB(nombreTablaDB, referenceColumnName,
                                    referenceValue, columnName, connection);

        } finally {
            UtilitariosJDBC.closeConnection(connection);
        }
    }


    private static String updateXRefRow(String tableGUID, String referenceColumnGUID,
                                String referenceValue, String columnGUID,
                                String value, String rowNumber) throws SQLException {
      
        String query;
        Connection connection;
        PreparedStatement updateStatement;
        Map queryArgMap = new HashMap(3);
        queryArgMap.put(":TABLE_NAME", tableGUID);
        queryArgMap.put(":REF_COLUMN_GUID", referenceColumnGUID);
        queryArgMap.put(":COLUMN_GUID", columnGUID);
        query =
                prepareQuery(" update :TABLE_NAME set :COLUMN_GUID = ?  , LAST_MODIFIED = ? where :REF_COLUMN_" +
                             "GUID = ?", queryArgMap);
        connection = getConnection();;
       

   
        updateStatement = connection.prepareStatement(query);
        Timestamp currentTime = new Timestamp((new Date()).getTime());
        Object params[] = { value, currentTime, referenceValue };
        int numOfRowsUpdated = executeUpdate(updateStatement, params);
        if (numOfRowsUpdated != 1) {
            throw new RuntimeException("Error Updating XREF");
        }
        return value;
    }


    private static String insertXRefRow(String tableGUID, String referenceColumnGUID, String referenceValue, String columnGUID, String value, String rowNumber, String mode) throws SQLException {
          Map queryArgMap;
          Connection connection;
          PreparedStatement insertStatement;
          queryArgMap = new HashMap(3);
          queryArgMap.put(":TABLE_NAME", tableGUID);
          queryArgMap.put(":REF_COLUMN_GUID", referenceColumnGUID);
          queryArgMap.put(":COLUMN_GUID", columnGUID);
          connection = null;
          insertStatement = null;
          String rownumber = GUIDGenerator.generateGUID();
          connection = getConnection();;
          if(rowNumber == null)
          {
              String query = prepareQuery(" insert into :TABLE_NAME (ROW_ID, :REF_COLUMN_GUID , :COLUMN_GUID , LAST_MODIFIED) values (?,?,?,?)", queryArgMap);
              insertStatement = connection.prepareStatement(query);
              
              Timestamp currentTime = new Timestamp((new Date()).getTime());
              Object params[] = {
                  rownumber, referenceValue, value, currentTime
              };
              executeUpdate(insertStatement, params);
          }
          return rownumber;
      }

    private static int executeUpdate(PreparedStatement preparedStatement,
                                     Object[] paramValues) throws SQLException {
        if (paramValues != null) {
            for (int i = 0; i < paramValues.length; i++) {
                preparedStatement.setObject(i + 1, paramValues[i]);

            }

        }
        return preparedStatement.executeUpdate();
    }

    private static String prepareQuery(String query, Map argumentList) {
        StringBuffer queryToReturn = new StringBuffer(query);
        for (int index = 0; (index = queryToReturn.indexOf(":", index)) != -1;) {
            int indexOfSpace = queryToReturn.indexOf(" ", index);
            String key = queryToReturn.substring(index, indexOfSpace);
            String value = (String)argumentList.get(key);
            queryToReturn.replace(index, indexOfSpace, value);
        }

        return queryToReturn.toString();
    }



    private static Connection getConnection() throws SQLException {
        return UtilitariosJDBC.getConnection(XREF_DS);
    }
    
/*     public static void main(String[] args){


           String string;

        try {
            string = lookupXRef("AddressId.xref",
            "SIEBEL"    ,
            "1-1Y6-837"    ,
             "CANONICAL"    ,
            true);
            System.out.println(string);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
  
    private static Connection getConnectionTest() throws SQLException {  
        Connection conn = null;
        try {
            Class.forName("oracle.jdbc.OracleDriver");
            //conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1531/SOADTAF","FILETRANSFER","PASSWORD");
            conn =
        DriverManager.getConnection("jdbc:oracle:thin:@172.22.164.158:1521/esbsrv2", "xref",
                            "xref");
        } catch (ClassNotFoundException e) {
            logger.error("Error instanciando el driver", e);
            throw new SQLException(e.getMessage());
        }
        return conn;
    } */
}
