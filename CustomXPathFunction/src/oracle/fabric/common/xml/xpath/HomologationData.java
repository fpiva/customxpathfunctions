package oracle.fabric.common.xml.xpath;

public class HomologationData {
    
    private String countryID;
    private String canonicalCode;
    private String homologatedConcept;
    private String homologatedCode;
    private String targetSystemCode;
    private int categoryID;
    
    public HomologationData() {
        super();
    }
    
    public void setCountryID(String countryID) {
        this.countryID = countryID;
    }
    
    public String getCountryID() {
        return countryID;
    }
    
    public void setCanonicalCode(String canonicalCode) {
        this.canonicalCode = canonicalCode;
    }
    
    public String getCanonicalCode() {
        return canonicalCode;
    }
    
    public void setHomologatedConcept(String homologatedConcept) {
        this.homologatedConcept = homologatedConcept;
    }
    
    public String getHomologatedConcept() {
        return homologatedConcept;
    }
    
    public void setHomologatedCode(String homologatedCode) {
        this.homologatedCode = homologatedCode;
    }
    
    public String getHomologatedCode() {
        return homologatedCode;
    }
    
    public void setTargetSystemCode(String targetSystemCode) {
        this.targetSystemCode = targetSystemCode;
    }
    
    public String getTargetSystemCode() {
        return targetSystemCode;
    }
    
    public void setCategoryID(int categoryID) {
        this.categoryID = categoryID;
    }
    
    public int getCategoryID() {
        return categoryID;
    }
}
